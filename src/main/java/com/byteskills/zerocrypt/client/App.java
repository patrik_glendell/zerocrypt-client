package com.byteskills.zerocrypt.client;

import com.byteskills.zerocrypt.client.storage.WatchEventProvider;
import com.byteskills.zerocrypt.client.storage.windows.WindowsConnector;
import com.byteskills.zerocrypt.client.synchronization.DefaultSynchronizationCoordinator;
import com.byteskills.zerocrypt.client.synchronization.DefaultSynchronizationService;
import com.byteskills.zerocrypt.client.synchronization.SynchronizationCoordinator;

import java.nio.file.Paths;

public class App 
{
	private static SynchronizationCoordinator coordinator;
	
    public static void main( String[] args )
    {
    	coordinator = new DefaultSynchronizationCoordinator();
    	coordinator.addService(new DefaultSynchronizationService(new WatchEventProvider("d:\\syncLab\\tempLocal", new WindowsConnector()), new WatchEventProvider("d:\\syncLab\\tempRemote", new WindowsConnector())));
    	coordinator.start();
        System.out.println("Services started, press any key to stop..");
        try { System.in.read(); } catch(Exception e) {}
        coordinator.stop();
        System.out.println( "Services stopped!" );
    }
}
