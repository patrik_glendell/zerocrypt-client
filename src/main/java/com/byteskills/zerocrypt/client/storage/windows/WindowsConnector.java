package com.byteskills.zerocrypt.client.storage.windows;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.ptr.IntByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byteskills.zerocrypt.client.storage.EventParameters;
import com.byteskills.zerocrypt.client.storage.NativeConnector;
import com.byteskills.zerocrypt.client.storage.windows.Kernel32Extended.BY_HANDLE_FILE_INFORMATION;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinNT.HANDLE;

public class WindowsConnector implements NativeConnector {

	public static enum ERROR_CODE { ERROR_SUCCESS, ERROR_INVALID_FUNCTION, ERROR_FILE_NOT_FOUND, ERROR_PATH_NOT_FOUND, ERROR_TOO_MANY_OPEN_FILES, ERROR_ACCESS_DENIED, ERROR_INVALID_HANDLE }

	private final Logger LOGGER = LoggerFactory.getLogger(WindowsConnector.class);

    private final int FSCTL_CREATE_OR_GET_OBJECT_ID = 0x900c0;
    private final int BUFFER_SIZE = 64;
	private final int GENERIC_READ = 0x80000000;
	private final int FILE_SHARE_READ = 0x00000001;
	private WinBase.SECURITY_ATTRIBUTES SECURITY_ATTRIBUTES = null;
	private final int OPEN_EXISTING = 3;
	private final int FILE_FLAG_BACKUP_SEMANTICS = 0x02000000;

	@Override
	public String getFileId(Path file) {
        BY_HANDLE_FILE_INFORMATION nfo = new BY_HANDLE_FILE_INFORMATION();
        HANDLE handle = getHandle(file.toString());
		verifyLastError("Error occurred while creating handle", file.toString(), Kernel32Extended.INSTANCE.GetLastError());
        Kernel32Extended.INSTANCE.GetFileInformationByHandle(handle, nfo);
		verifyLastError("Error occurred while getting file information", file.toString(), Kernel32Extended.INSTANCE.GetLastError());
        String identifier = nfo.nFileIndexHigh + nfo.nFileIndexLow.toString() + Integer.toHexString(nfo.dwVolumeSerialNumber.intValue());
		Kernel32Extended.INSTANCE.CloseHandle(handle);
        return identifier;
	}

	private void verifyLastError(String desc, String file, int err) {
		if(err != 0) {
			LOGGER.error(desc + " for '" + file + "' (" + (err < 7 ? ERROR_CODE.values()[err] : err) + ")");
		}
	}

    private HANDLE getHandle(String file) {
        return Kernel32Extended.INSTANCE.CreateFile(file, GENERIC_READ, FILE_SHARE_READ, SECURITY_ATTRIBUTES, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, null);
    }

	public String getFileId2(Path file) {
        Pointer bufferPrt = new Memory(BUFFER_SIZE);
        IntByReference uidOut = new IntByReference(0);
        HANDLE handle = getHandle(file.toString());
        Kernel32Extended.INSTANCE.DeviceIoControl(handle, FSCTL_CREATE_OR_GET_OBJECT_ID, null, 0, bufferPrt, BUFFER_SIZE, uidOut, null);
        verifyLastError("Error occurred while FSCTL_CREATE_OR_GET_OBJECT_ID", file.toString(), Kernel32Extended.INSTANCE.GetLastError());
        Kernel32Extended.INSTANCE.CloseHandle(handle);
        return Integer.toHexString(bufferPrt.getInt(0));
	}
	
	@Override
	public Map<String, EventParameters<Path>> getChildren(Path watchDirectory) throws IOException {
		Map<String, EventParameters<Path>> map = new HashMap<>();
		Files.walkFileTree(watchDirectory, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				map.put(getFileId(file), new EventParameters<>(watchDirectory.relativize(file), null, file.toFile().lastModified(), 0, null));
				return FileVisitResult.CONTINUE;
			}
			
			@Override
			public FileVisitResult visitFileFailed(Path arg0, IOException arg1) throws IOException {
				LOGGER.error("Invalid / inaccessible object: " + arg1.getMessage() + " (" + arg1.getStackTrace() + ")");
				return FileVisitResult.CONTINUE;
			}
		});
		return map;
	}
}
