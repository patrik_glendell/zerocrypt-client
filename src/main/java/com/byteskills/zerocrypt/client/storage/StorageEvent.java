package com.byteskills.zerocrypt.client.storage;

public interface StorageEvent {
	enum EventType { CREATE, MODIFY, DELETE, RENAME, MOVE };
	Parcel getParcel();
	EventType getEventType();
}
