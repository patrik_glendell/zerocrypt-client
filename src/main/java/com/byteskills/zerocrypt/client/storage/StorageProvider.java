package com.byteskills.zerocrypt.client.storage;

import java.util.Map;
import com.byteskills.zerocrypt.client.Startable;

public interface StorageProvider extends Startable, StorageListener {
	Map<String, Parcel> getInventory();
	void addListener(StorageListener listener);
	void removeListener(StorageListener listener); 
}
