package com.byteskills.zerocrypt.client.storage;

import java.util.List;

public interface StorageListener {
	void receiveEvents(List<StorageEvent> events);
}
