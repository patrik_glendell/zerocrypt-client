package com.byteskills.zerocrypt.client.storage;

public class DefaultStorageEvent implements StorageEvent {
	private final EventType type;
	protected Parcel parcel;

	public DefaultStorageEvent(EventType type, Parcel parcel) {
		this.type = type;
		this.parcel = parcel;
	}
	
	@Override
	public EventType getEventType() {
		return type;
	}
	
	@Override
	public Parcel getParcel() {
		return parcel;
	}
}
