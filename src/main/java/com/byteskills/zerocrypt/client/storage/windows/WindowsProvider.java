package com.byteskills.zerocrypt.client.storage.windows;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.AbstractMap.SimpleEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byteskills.zerocrypt.client.storage.DefaultStorageEvent;
import com.byteskills.zerocrypt.client.storage.EventParameters;
import com.byteskills.zerocrypt.client.storage.Parcel;
import com.byteskills.zerocrypt.client.storage.StorageEvent;
import com.byteskills.zerocrypt.client.storage.StorageEvent.EventType;
import com.byteskills.zerocrypt.client.storage.StorageListener;
import com.byteskills.zerocrypt.client.storage.StorageProvider;

public class WindowsProvider {
    
}
