package com.byteskills.zerocrypt.client.storage;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byteskills.zerocrypt.client.storage.StorageEvent.EventType;
import com.byteskills.zerocrypt.client.storage.windows.WindowsParcel;

public class WatchEventProvider_Old implements StorageProvider {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(WatchEventProvider_Old.class);
	
	private WatchService watchService;
	private final NativeConnector nativeConnector;
	private final ExecutorService fixedExecutor = Executors.newCachedThreadPool();
	private final ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);
	private final List<StorageListener> listeners = new ArrayList<>();
	private final Path watchDirectory;
	
	private Map<String, EventParameters<Path>> watchCache; //Cache containing current represented content of the watchDirectory
	private final AbstractQueue<StorageEvent> inQueue = new ConcurrentLinkedQueue<>(); //Queue containing incoming parcels
	//private Map<String, EventParameters<Path>> outQueue = new LinkedHashMap<>(); //Queue containing outgoing events
	private final AbstractQueue<EventParameters<Path>> outQueue = new ConcurrentLinkedQueue<>(); //Queue containing outgoing events
	private final long outQueueInterval = 2000; //MS
	
	private AtomicBoolean processingInQueue = new AtomicBoolean(false);
	private AtomicBoolean running = new AtomicBoolean(false);
	
	
	
	public WatchEventProvider_Old(String watchDirectory, NativeConnector nativeConnector) {
		this.watchDirectory = Paths.get(watchDirectory);
		this.nativeConnector = nativeConnector;
	}

	/**
	 * Start listening to directory events
	 * @return whether or it started successfully
	 */
	@Override
	@SuppressWarnings("restriction")
	public boolean start() {
		if(running.compareAndSet(false, true)) { 
			try { //TODO: LOCK USER ACCESS TO FOLDER
				watchCache = nativeConnector.getChildren(watchDirectory);
				LOGGER.debug("Initial directory synchronization completed, " + watchCache.size() +" items added");
				watchService = FileSystems.getDefault().newWatchService();
				watchDirectory.register(
						watchService, 
						new WatchEvent.Kind<?>[]{ StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.OVERFLOW}, 
						com.sun.nio.file.ExtendedWatchEventModifier.FILE_TREE);
				fixedExecutor.execute(() -> { listenForWatchEvents(); });
				LOGGER.info("[" + watchDirectory.toString() + "] Started WindowsProvider");
				return true;
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			} //TODO: RELEASE USER ACCESS TO FOLDER
		} else {
			LOGGER.info("[" + watchDirectory.toString() + "] WindowsProvider already started");
		}
		return false; 
	}
	
	/**
	 * Stops listening to directory events
	 * @return whether or it stopped successfully
	 */
	@Override
	public boolean stop() {
		try {
			watchService.close();
			scheduledExecutor.shutdown();
			fixedExecutor.shutdown();
			running.set(false);
			LOGGER.info("[" + watchDirectory.toString() + "] Stopped WindowsProvider");
			return true;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}
	
	/**
	 * Restart the watchEvent listening process
	 */
	private void restart() {
		throw new UnsupportedOperationException();
	}
	
	//--------- Handles events triggered locally by the watchService ---------
	
	/**
	 * Continuously retrieves watchKeys from the watchService
	 */
	private void listenForWatchEvents() {
		scheduledExecutor.scheduleWithFixedDelay(() -> { processOutQueue(); }, outQueueInterval, outQueueInterval, TimeUnit.MILLISECONDS);
		while(running.get()) {
			try {
				handleWatchKey(watchService.take());
			} catch(ClosedWatchServiceException e) { //need running = false?
				LOGGER.debug("[" + watchDirectory.toString() + "] Watchservice was closed");
				if(running.get()) {
					restart();
				}
			} catch (InterruptedException e) {
				LOGGER.warn(e.getMessage());
				restart();
			}
		}
	}
	
	private void handleWatchKey(WatchKey key) {
		handleWatchKeyEvents(key.pollEvents());
		if(!key.reset()) {
			LOGGER.error("Watchkey invalid!"); //TODO: most likely a service restart should occur here
		}
	}
	
	private void handleWatchKeyEvents(List<WatchEvent<?>> events) {
		for(WatchEvent<?> event : events) {
			String uid = nativeConnector.getFileId(watchDirectory.resolve((Path) event.context()));
			System.out.println("######Received event: [" + uid + "]" + (Path)event.context() + " - " + event.kind().name());
			switch(event.kind().name()) {
				case "ENTRY_DELETE": 
					outQueue.add(new EventParameters<>((Path)event.context(), null, EventType.DELETE, System.currentTimeMillis(), uid));
					break;
				case "ENTRY_CREATE": 
					EventParameters<Path> storedParams = watchCache.get(uid);
					System.out.println("Stored item for this: " + storedParams.getPath());
					Path eventPath = (Path)event.context();
					if(storedParams != null) {
						removeDeletedFromOutQueue(storedParams.getPath()); //remove potential delete requests connected to this create (since its a ignore, move or rename operation)
						if(
								(eventPath.getParent() != storedParams.getPath().getParent()) ||
								(eventPath.getParent() != null && storedParams.getPath().getParent() != null && !storedParams.getPath().getParent().equals(eventPath.getParent())) ) {
							outQueue.add(new EventParameters<>((Path)event.context(), storedParams.getPath(), EventType.MOVE, System.currentTimeMillis(), uid));
						}else if(!storedParams.getPath().getFileName().equals(eventPath.getFileName())) {
							outQueue.add(new EventParameters<>((Path)event.context(), storedParams.getPath(), EventType.RENAME, System.currentTimeMillis(), uid));
						}
					} else {
						outQueue.add(new EventParameters<>((Path)event.context(), null, EventType.CREATE, System.currentTimeMillis(), uid));
					}
					break;
				case "ENTRY_MODIFY":
					if(!Files.isDirectory((Path)event.context())) { //MODIFY for folders are irrelevant
						outQueue.add(new EventParameters<>((Path)event.context(), null, EventType.MODIFY, System.currentTimeMillis(), uid));
					}
					break;
				case "OVERFLOW": LOGGER.error("[" + watchDirectory.toString() + "] Tracking overflow occured, file changes lost!"); break; //TODO: Rescan drive
				default: LOGGER.debug("[" + watchDirectory.toString() + "] Drive event discarded (type not handled)"); break;
			}
		}
	}
	
	/**
	 * Process the outQueue in intervals, only sending items that has been in the queue for x amount of time.
	 * This is done in order to securely handle file rename and moves.
	 */
	private void processOutQueue() {
		long currentTime = System.currentTimeMillis();
		for(Iterator<EventParameters<Path>> outQueueIt = outQueue.iterator(); outQueueIt.hasNext();) {
			EventParameters<Path> params = outQueueIt.next();
			if(params.getOccured() <= currentTime-1000) {
				LOGGER.debug("OUT: [" + params.getUid() + "] " + params.getPath() + " - " + params.getEventType() + " (" + params.getLegacyPath() + ")");
				outQueueIt.remove();
			}
		}
	}
	
	private void removeDeletedFromOutQueue(Path path) {
		for(Iterator<EventParameters<Path>> outQueueIt = outQueue.iterator(); outQueueIt.hasNext();) {
			EventParameters<Path> params = outQueueIt.next();
			if(params.getPath().equals(path) && params.getEventType().equals(EventType.DELETE)) {
				outQueueIt.remove();
			}
		}
	}

	//--------- Handles StorageEvents received by other peers ---------

	@Override
	public void receiveEvents(List<StorageEvent> events) {
		inQueue.addAll(events);
		LOGGER.debug("[" + watchDirectory.toString() + "] " + events.size() + " events added to event-queue");
		if(processingInQueue.compareAndSet(false, true)) {
			fixedExecutor.execute(() -> { processInQueue(); });
		} else {
			LOGGER.debug("[" + watchDirectory.toString() + "] Method 'processEventQueue' already running");
		}
	}
	
	private void processInQueue() {
		Iterator<StorageEvent> items = inQueue.iterator();
		while(items.hasNext()) {
			StorageEvent event = items.next();
			LOGGER.debug("[" + watchDirectory.toString() + "] Received remote event for '" + event.getParcel().getPathAsString() + "', mimeType '" + event.getParcel().getMimeType() + "', eventType '" + event.getEventType() + "'");
			//process event
			items.remove();
		}
		processingInQueue.set(false);
	}

	@Override
	public Map<String, Parcel> getInventory() {
		Map<String, Parcel> parcels = new HashMap<String, Parcel>();
		for(Entry<String, EventParameters<Path>> entry : watchCache.entrySet()) {
			try {
				WindowsParcel parcel = new WindowsParcel(watchDirectory, entry.getValue().getPath(), null);
				parcels.put(parcel.getPathAsString(), parcel);
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}
		}
		return parcels;
	}

	@Override
	public void addListener(StorageListener listener) {
		if(!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	@Override
	public void removeListener(StorageListener listener) {
		if(listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}
	
}
