package com.byteskills.zerocrypt.client.storage;

import java.util.Arrays;

public abstract class ParcelBase implements Parcel {

	private String[] path;
	private String[] legacyPath;
	private String mimeType;
	private String version;
	private long created;
	private long modified;

	/**
	 * Set the parameters
	 */
	protected void setParameters(String[] path, String mimeType, String version, long created, long modified, String[] legacyPath) {
		this.path = path;
		this.mimeType = mimeType;
		this.version = version;
		this.created = created;
		this.modified = modified;
		this.legacyPath = legacyPath;
	}

	/**
	 * @return the modified date as milliseconds
	 */
	public long getModified() {
		return modified;
	}

	/**
	 * @return the path as an array
	 */
	public String[] getPath() {
		return path;
	}
	
	/**
	 * @return the path of the object BEFORE move and / or rename occured as an array
	 */
	public String[] getLegacyPath() {
		return legacyPath;
	}
	
	/**
	 * @return the path as a forward-slash delimited string . 
	 * Reason is to provide the same structure independent of O/S or cloud provider (for comparability).
	 */
	public String getPathAsString() {
		String pathStr = path[0];
		for(int i = 1; i < path.length; i++) {
			pathStr += "/" + path[i];
		}
		return pathStr;
	}
	
	/**
	 * @return true if the path to compare matches the parcel path
	 */
	public boolean compare(String[] pathToCompare) {
		return Arrays.equals(path, pathToCompare);
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the created date as milliseconds
	 */
	public long getCreated() {
		return created;
	}
}
