package com.byteskills.zerocrypt.client.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class WatchEventProvider implements StorageProvider, Runnable {

    private final static Logger LOGGER = LoggerFactory.getLogger(WatchEventProvider.class);
    private final Path watchDirectory;
    private WatchService watchService;
    private final NativeConnector nativeConnector;

    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final List<StorageListener> listeners = new ArrayList<>();

    private AtomicBoolean processingInQueue = new AtomicBoolean(false);
    private AtomicBoolean running = new AtomicBoolean(false);


    public WatchEventProvider(String watchDirectory, NativeConnector nativeConnector) {
        this.watchDirectory = Paths.get(watchDirectory);
        this.nativeConnector = nativeConnector;
    }

    @Override
    public boolean start() {
        if(running.compareAndSet(false, true)) {
            try { //TODO: LOCK USER ACCESS TO FOLDER
                watchService = FileSystems.getDefault().newWatchService();
                watchDirectory.register(
                        watchService,
                        new WatchEvent.Kind<?>[]{ StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.OVERFLOW},
                        com.sun.nio.file.ExtendedWatchEventModifier.FILE_TREE);
                executor.execute(this);
                LOGGER.info("[" + watchDirectory.toString() + "] Started WindowsProvider");
                return true;
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            } //TODO: RELEASE USER ACCESS TO FOLDER
        } else {
            LOGGER.info("[" + watchDirectory.toString() + "] WindowsProvider already started");
        }
        return false;
    }

    @Override
    public Map<String, Parcel> getInventory() {
        return new HashMap<>();
    }

    //------------- Directory monitoring -------------

    @Override
    public void run() {
        while(running.get()) {
            try {
                handleWatchKey(watchService.take());
            } catch(ClosedWatchServiceException e) { //need running = false?
                LOGGER.debug("[" + watchDirectory.toString() + "] Watchservice was closed");
                if(running.get()) {
                    restart();
                }
            } catch (InterruptedException e) {
                LOGGER.warn(e.getMessage());
                restart();
            }
        }
    }

    private void handleWatchKey(WatchKey key) {
        handleWatchEvents(key.pollEvents());
        if(!key.reset()) {
            LOGGER.error("Watchkey invalid!"); //TODO:  most likely a service restart should occur here, followed by failure if not successful
        }
    }

    private void handleWatchEvents(List<WatchEvent<?>> events) {
        for(WatchEvent<?> event : events) {
            String fileId = nativeConnector.getFileId(watchDirectory.resolve((Path) event.context()));
            System.out.println("######Received event: [" + fileId + "]" + event.context() + " - " + event.kind().name());
            switch(event.kind().name()) {
                case "ENTRY_DELETE":
                    break;
                case "ENTRY_CREATE":
                    break;
                case "ENTRY_MODIFY":
                    break;
                case "OVERFLOW": LOGGER.error("[" + watchDirectory.toString() + "] Tracking overflow occured, file changes lost!"); break; //TODO: Rescan drive
                default: LOGGER.debug("[" + watchDirectory.toString() + "] Drive event discarded (type not handled)"); break;
            }
        }
    }

    //------------- Processing received external events -------------

    @Override
    public void receiveEvents(List<StorageEvent> events) {

    }

    @Override
    public boolean stop() {
        try {
            watchService.close();
            executor.shutdown();
            executor.shutdown();
            running.set(false);
            LOGGER.info("[" + watchDirectory.toString() + "] Stopped WindowsProvider");
            return true;
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    private void restart() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addListener(StorageListener listener) {
        if(!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(StorageListener listener) {
        if(listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }
}
