package com.byteskills.zerocrypt.client.storage;

import com.byteskills.zerocrypt.client.storage.StorageEvent.EventType;

public class EventParameters <T> {
	private final T path;
	private final T legacyPath;
	private final EventType eventType;
	private final long version;
	private final long occured;
	private final String uid;
	
	public EventParameters(T path, T legacyPath, long version, long occured, String uid) {
		this.path = path;
		this.legacyPath = legacyPath;
		eventType = null;
		this.version = version;
		this.occured = occured;
		this.uid = uid;
	}
	
	public EventParameters(T file, T legacyFile, EventType eventType, long occured, String uid) {
		this.path = file;
		this.legacyPath = legacyFile;
		this.eventType = eventType;
		version = 0;
		this.occured = occured;
		this.uid = uid;
	}
	
	/**
	 * @return the file
	 */
	public T getPath() {
		return path;
	}
	/**
	 * @return the legacyFile
	 */
	public T getLegacyPath() {
		return legacyPath;
	}
	/**
	 * @return the eventType
	 */
	public EventType getEventType() {
		return eventType;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @return the occured
	 */
	public long getOccured() {
		return occured;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
}
