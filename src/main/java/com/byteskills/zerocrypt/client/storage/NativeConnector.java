package com.byteskills.zerocrypt.client.storage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

public interface NativeConnector {
	String getFileId(Path file);
	Map<String, EventParameters<Path>> getChildren(Path watchDirectory) throws IOException;
}
