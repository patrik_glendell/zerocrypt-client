package com.byteskills.zerocrypt.client.storage;

import java.io.OutputStream;

import com.byteskills.zerocrypt.client.security.EncryptionService;

public interface Parcel {
	enum WriteMode { ENCRYPT, DECRYPT };
	String write(OutputStream destination, WriteMode mode, EncryptionService encryptionService); //NEEDS TO BE THREAD SAFE!?
	boolean isFolder();
	long getModified();
	String[] getPath();
	String[] getLegacyPath();
	String getPathAsString();
	boolean compare(String[] path);
	String getMimeType();
	String getVersion();
	long getCreated();
	boolean exists();
}
