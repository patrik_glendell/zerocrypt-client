package com.byteskills.zerocrypt.client.storage.windows;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byteskills.zerocrypt.client.synchronization.DefaultSynchronizationService;

public class WindowsFileVisitor extends SimpleFileVisitor<Path> {

	private final static Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationService.class);
	
	@Override
	public FileVisitResult visitFileFailed(Path arg0, IOException arg1) throws IOException {
		LOGGER.debug(arg1.getMessage());
		return null;
	}

}
