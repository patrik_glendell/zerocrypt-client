package com.byteskills.zerocrypt.client.storage.windows;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIFunctionMapper;
import com.sun.jna.win32.W32APITypeMapper;

public interface Kernel32Extended extends StdCallLibrary {
    final static Map<String, Object> WIN32API_OPTIONS = new HashMap<String, Object>() {
        private static final long serialVersionUID = 1L;
        {
            put(Library.OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);
            put(Library.OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
        }
    };

    public Kernel32Extended INSTANCE = (Kernel32Extended) Native.loadLibrary("Kernel32", Kernel32Extended.class, WIN32API_OPTIONS);

    public int GetLastError();

    public class BY_HANDLE_FILE_INFORMATION extends Structure {
        public DWORD    dwFileAttributes;
        public FILETIME ftCreationTime;
        public FILETIME ftLastAccessTime;
        public FILETIME ftLastWriteTime;
        public DWORD    dwVolumeSerialNumber;
        public DWORD    nFileSizeHigh;
        public DWORD    nFileSizeLow;
        public DWORD    nNumberOfLinks;
        public DWORD    nFileIndexHigh;
        public DWORD    nFileIndexLow;

		public List<String> getFieldOrder() {
			return Arrays.asList(
				"dwFileAttributes", 
				"ftCreationTime", 
				"ftLastAccessTime", 
				"ftLastWriteTime",
				"dwVolumeSerialNumber",
				"nFileSizeHigh",
				"nFileSizeLow",
				"nNumberOfLinks",
				"nFileIndexHigh",
				"nFileIndexLow"
			);
		};  
    }; 

    boolean GetFileInformationByHandle(HANDLE hFile, BY_HANDLE_FILE_INFORMATION lpFileInformation); //TODO: ID not unique on ReFS, potentially need GetFileInformationByHandleEx
    //boolean GetFileInformationByHandleEx(HANDLE hFile, FILE_INFO_BY_HANDLE_CLASS FileInformationClass, WinDef.LPVOID lpFileInformation, DWORD dwBufferSize);

    HANDLE CreateFile(
            java.lang.String arg0,
            int arg1,
            int arg2,
            WinBase.SECURITY_ATTRIBUTES arg3,
            int arg4,
            int arg5,
            HANDLE arg6);

    boolean CloseHandle(HANDLE hObject);

    boolean DeviceIoControl(HANDLE hDevice,
                            int dwIoControlCode,
                            Pointer lpInBuffer,
                            int nInBufferSize,
                            Pointer lpOutBuffer,
                            int nOutBufferSize,
                            IntByReference lpBytesReturned,
                            Pointer lpOverlapped);
}
