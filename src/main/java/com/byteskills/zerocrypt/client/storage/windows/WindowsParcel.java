package com.byteskills.zerocrypt.client.storage.windows;

import java.io.IOException;
import java.io.OutputStream;

import com.byteskills.zerocrypt.client.security.EncryptionService;
import com.byteskills.zerocrypt.client.storage.ParcelBase;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;

public class WindowsParcel extends ParcelBase {

	private final Path path;

	public WindowsParcel(Path watchDirectory, Path relativePath, Path relativeLegacyPath) throws IOException {
		path = watchDirectory.resolve(relativePath);
		
		if(Files.exists(path)) {
			BasicFileAttributes attributes = Files.readAttributes(path, BasicFileAttributes.class);
			setParameters(
					toPath(relativePath),
					Files.probeContentType(path), //mimeType
					null, //version :: if not modified date is to be used
					attributes.creationTime().toMillis(),
					attributes.lastModifiedTime().toMillis(),
					toPath(relativeLegacyPath)
					);
		} else {
			setParameters(toPath(relativePath),null, null, 0, 0, toPath(relativeLegacyPath));
		}
	}

	@Override
	public String write(OutputStream destination, WriteMode mode, EncryptionService encryptionService) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isFolder() {
		return Files.isDirectory(path);
	}
	
	private String[] toPath(Path relativePath) {
		if(relativePath == null) {
			return null;
		}

		Iterator<Path> pathIt = relativePath.iterator();
		String[] path = new String[relativePath.getNameCount()];
		for(int i = 0; i < path.length; i++) {
			path[i] = pathIt.next().toString();
		}
		return path;
	}

	@Override
	public boolean exists() {
		return Files.exists(path);
	}
}
