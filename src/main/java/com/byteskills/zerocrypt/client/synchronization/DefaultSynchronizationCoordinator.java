package com.byteskills.zerocrypt.client.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultSynchronizationCoordinator implements SynchronizationCoordinator {

	private final static Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationCoordinator.class);
	private final List<SynchronizationService> services = new ArrayList<>();
	
	@Override
	public boolean start() {
		boolean status = true;
		for(SynchronizationService service : services) {
			if(!service.isStarted()) {
				status = service.start();
			}
		}
		return status;
	}

	@Override
	public boolean stop() {
		boolean status = true;
		for(SynchronizationService service : services) {
			if(service.isStarted()) {
				status = service.stop();
			}
		}
		return status;
	}

	@Override
	public void addService(SynchronizationService service) {
		services.add(service);
	}

	@Override
	public void executeService(SynchronizationService service) {
		services.add(service);
		if(!service.isStarted()) {
			service.start();
		}
	}

}
