package com.byteskills.zerocrypt.client.synchronization;

import com.byteskills.zerocrypt.client.Startable;

/**
 * Manages a collection of active synchronization services.
 * @author Daniel Ryhle
 *
 */
public interface SynchronizationCoordinator extends Startable {
	void addService(SynchronizationService service);
	void executeService(SynchronizationService service);
}
