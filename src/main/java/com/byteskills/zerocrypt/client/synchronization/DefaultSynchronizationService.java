package com.byteskills.zerocrypt.client.synchronization;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byteskills.zerocrypt.client.storage.Parcel;
import com.byteskills.zerocrypt.client.storage.StorageProvider;

public class DefaultSynchronizationService implements SynchronizationService {

	private final static Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationService.class);
	private final StorageProvider remoteProvider;
	private final StorageProvider localProvider;
	private long timeStarted = 0;
	
	public DefaultSynchronizationService(StorageProvider localProvider, StorageProvider remoteProvider) {
		this.localProvider = localProvider;
		this.remoteProvider = remoteProvider;
	}
	
	public boolean start() {
		timeStarted = (new Date()).getTime();
		localProvider.addListener(remoteProvider);
		remoteProvider.addListener(localProvider);
		boolean started = localProvider.start() && remoteProvider.start();
		
		//##### TEMP listing inventory packages
		Map<String, Parcel> localParcels = localProvider.getInventory();
		for(Entry<String, Parcel> entry : localParcels.entrySet()) {
				System.out.println("Local entry: " + entry.getKey());
		}
		Map<String, Parcel> remoteParcels = remoteProvider.getInventory();
		for(Entry<String, Parcel> entry : remoteParcels.entrySet()) {
			System.out.println("Remote entry: " + entry.getKey());
		}
		
		return started;
	}

	public boolean stop() {
		return localProvider.stop() && remoteProvider.stop();
	}

	@Override
	public boolean isStarted() {
		return timeStarted > 0;
	}

}
