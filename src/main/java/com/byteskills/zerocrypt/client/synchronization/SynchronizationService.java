package com.byteskills.zerocrypt.client.synchronization;

import com.byteskills.zerocrypt.client.Startable;

public interface SynchronizationService extends Startable {
	boolean isStarted();
}
