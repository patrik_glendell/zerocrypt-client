package com.byteskills.zerocrypt.client;

public interface Startable {
	boolean start();
	boolean stop();
}
